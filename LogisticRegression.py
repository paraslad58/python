# -*- coding: utf-8 -*-
"""
Created on Tue Jan  4 17:05:43 2022

@author: Paras
"""

import numpy as np
import pandas as pd

# capturing data from the txt file of dataset
def Dataextract(file):
    my_file = open(file)
    content = my_file.readlines()
    content = [x.strip() for x in content]

    l1 = []

    for i in content:
        l = i.split(",")
        l1.append(l)

    #formulating each input and output in unique list from dataset
    leng1 = len(l1)
    leng2 = len(l1[1])
    temp = []
    hum = []
    light = []
    co2 = []
    humratio = []
    occ = []

    for i in range(1,leng1):
        for j in range(2,leng2):
            if j == 2:
                temp.append(float(l1[i][j]))
            if j==3:
                hum.append(float(l1[i][j]))
            if j==4:
                light.append(float(l1[i][j]))
            if j==5:
                co2.append(float(l1[i][j]))
            if j==6:
                humratio.append(float(l1[i][j]))
            if j==7:
                occ.append(float(l1[i][j]))

    # normalization od data to range 0 to 1            
    l_temp = min(temp)
    m_temp = max(temp)

    l_hum = min(hum)
    m_hum = max(hum)

    l_light = min(light)
    m_light = max(light)

    l_co2 = min(co2)
    m_co2 = max(co2)

    l_humratio = min(humratio)
    m_humratio = max(humratio)

    new_max = 1
    new_min = 0

    for i in range(len(temp)):
        temp[i] = (((temp[i]-l_temp)/(m_temp-l_temp))*(new_max-new_min))+new_min
        hum[i] = (((hum[i]-l_hum)/(m_hum-l_hum))*(new_max-new_min))+new_min
        light[i] = (((light[i]-l_light)/(m_light-l_light))*(new_max-new_min))+new_min
        co2[i] = (((co2[i]-l_co2)/(m_co2-l_co2))*(new_max-new_min))+new_min
        humratio[i] = (((humratio[i]-l_humratio)/(m_humratio-l_humratio))*(new_max-new_min))+new_min
    data_trn = []
    for i in range(len(temp)):
        u = [temp[i],hum[i],light[i],co2[i],humratio[i],occ[i]]
        data_trn.append(u)

    data_trn = np.array(data_trn)
    data = { "Temperature" : temp, "Humidity" : hum, "Light" : light, "CO2" : co2, "Humidity Ratio" : humratio}
    return(data_trn,data)



#Fetching Training data 
data_trn,data = Dataextract("datatraining.txt")
data_trnsamp = []
data_trnlab = []
for i in data_trn:
    data_trnsamp.append(i[:-1])
    #data_trnsamp.append([i[0],i[1],i[3]])
    data_trnlab.append([i[-1]])
data_trnsamp =  np.array(data_trnsamp)
data_trnlab = np.array(data_trnlab)



#Fetching Testing data when door is closed 
data_test,data1 = Dataextract("datatest.txt")
data_testsamp = []
data_testlab = []
for i in data_test:
    data_testsamp.append(i[:-1])
    #data_testsamp.append([i[0],i[1],i[3]])
    data_testlab.append([i[-1]])
data_testsamp =  np.array(data_testsamp)
data_testlab = np.array(data_testlab)


#Fetching Testing data when door is open
data_test2,data2 = Dataextract("datatest2.txt")
data_testsamp2 = []
data_testlab2 = []
for i in data_test2:
    data_testsamp2.append(i[:-1])
    #data_testsamp2.append([i[0],i[1],i[3]])
    data_testlab2.append([i[-1]])
data_testsamp2 =  np.array(data_testsamp2)
data_testlab2 = np.array(data_testlab2)



#plotting correlation matrix
df = pd.DataFrame(data,columns=['Temperature','Humidity','Light','CO2','Humidity Ratio'])
print(df.corr())


#Logistic Regression Class
class LogisticRegression():
    def fit(self,X,Y,B):
        a = np.dot(X,B)
        P = 1 / (1 + np.exp(-a))
        W = np.zeros((P.shape[0],P.shape[0]))
        for i in range(len(P)):
            for j in range(len(P)):
                if i == j:
                    W[i][j] = P[i]*(1-P[i])
        H = X.T.dot(W).dot(X);
        G = np.dot(X.T, (Y-P));
        up = np.dot(np.linalg.pinv(H), G)
        B = B + up
        return B;
    def predict(self,X,B):
        Y = []
        for i in range(len(X)):
            a = np.dot(X[i].T,B)
            out = 1/ (1+np.exp(-a))
            if(out<0.5):
                Y.append(0)
            else:
                Y.append(1)
        return Y

#calculating accuracy
def accuracy(pred, data):
    mis_classified = 0
    for i in range(len(data)):
        if (data[i][0] != pred[i]):
            mis_classified +=1
    acc = 1 - mis_classified/len(data)
    return(acc)



#Performing Logistic Regression over our dataset

b = np.zeros(data_trnsamp.shape[1])
B = []
for i in b:
    B.append([i])
B = np.array(B)
lr = LogisticRegression()
for i in range(20):
    B = lr.fit(data_trnsamp,data_trnlab,B)



out_pred= lr.predict(data_trnsamp,B)
out_trn = accuracy(out_pred,data_trnlab)
out_pred= lr.predict(data_testsamp,B)
out_test1 = accuracy(out_pred,data_testlab)
out_pred= lr.predict(data_testsamp2,B)
out_test2 = accuracy(out_pred,data_testlab2)
print("Logistic Regression")
print("Training Accuracy : ",out_trn)
print("Testing Accuracy when door is closed: ",out_test1)
print("Testing accuracy when door is open: ",out_test2)
