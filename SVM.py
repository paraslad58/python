# -*- coding: utf-8 -*-
"""
Created on Tue Jan  4 17:10:27 2022

@author: Paras
"""

import numpy as np
import pandas as pd


# capturing data from the txt file of dataset
def Dataextract(file):
    my_file = open(file)
    content = my_file.readlines()
    content = [x.strip() for x in content]

    l1 = []

    for i in content:
        l = i.split(",")
        l1.append(l)

    #formulating each input and output in unique list from dataset
    leng1 = len(l1)
    leng2 = len(l1[1])
    temp = []
    hum = []
    light = []
    co2 = []
    humratio = []
    occ = []

    for i in range(1,leng1):
        for j in range(2,leng2):
            if j == 2:
                temp.append(float(l1[i][j]))
            if j==3:
                hum.append(float(l1[i][j]))
            if j==4:
                light.append(float(l1[i][j]))
            if j==5:
                co2.append(float(l1[i][j]))
            if j==6:
                humratio.append(float(l1[i][j]))
            if j==7:
                occ.append(float(l1[i][j]))

    # normalization od data to range 0 to 1            
    l_temp = min(temp)
    m_temp = max(temp)

    l_hum = min(hum)
    m_hum = max(hum)

    l_light = min(light)
    m_light = max(light)

    l_co2 = min(co2)
    m_co2 = max(co2)

    l_humratio = min(humratio)
    m_humratio = max(humratio)

    new_max = 1
    new_min = 0

    for i in range(len(temp)):
        temp[i] = (((temp[i]-l_temp)/(m_temp-l_temp))*(new_max-new_min))+new_min
        hum[i] = (((hum[i]-l_hum)/(m_hum-l_hum))*(new_max-new_min))+new_min
        light[i] = (((light[i]-l_light)/(m_light-l_light))*(new_max-new_min))+new_min
        co2[i] = (((co2[i]-l_co2)/(m_co2-l_co2))*(new_max-new_min))+new_min
        humratio[i] = (((humratio[i]-l_humratio)/(m_humratio-l_humratio))*(new_max-new_min))+new_min
    data_trn = []
    for i in range(len(temp)):
        u = [temp[i],hum[i],light[i],co2[i],humratio[i],occ[i]]
        data_trn.append(u)

    data_trn = np.array(data_trn)
    data = { "Temperature" : temp, "Humidity" : hum, "Light" : light, "CO2" : co2, "Humidity Ratio" : humratio}
    return(data_trn,data)



#Fetching Training data 
data_trn,data = Dataextract("datatraining.txt")
data_trnsamp = []
data_trnlab = []
for i in data_trn:
    data_trnsamp.append(i[:-1])
    #data_trnsamp.append([i[0],i[1],i[3]])
    data_trnlab.append([i[-1]])
data_trnsamp =  np.array(data_trnsamp)
data_trnlab = np.array(data_trnlab)



#Fetching Testing data when door is closed 
data_test,data1 = Dataextract("datatest.txt")
data_testsamp = []
data_testlab = []
for i in data_test:
    data_testsamp.append(i[:-1])
    #data_testsamp.append([i[0],i[1],i[3]])
    data_testlab.append([i[-1]])
data_testsamp =  np.array(data_testsamp)
data_testlab = np.array(data_testlab)


#Fetching Testing data when door is open
data_test2,data2 = Dataextract("datatest2.txt")
data_testsamp2 = []
data_testlab2 = []
for i in data_test2:
    data_testsamp2.append(i[:-1])
    #data_testsamp2.append([i[0],i[1],i[3]])
    data_testlab2.append([i[-1]])
data_testsamp2 =  np.array(data_testsamp2)
data_testlab2 = np.array(data_testlab2)

#SVM Classifier
class SVM:
    def __init__(self, LR=0.001, Lambda=0.001, Iter=500):
        self.lr = LR
        self.Lambda = Lambda
        self.iters = Iter
        self.W = None
        self.B = None


    def fit(self, X, Y):
        data, features = X.shape
        
        up_y = np.where(Y <= 0, -1, 1)
        
        self.W = np.zeros(features)
        self.B = 0

        for _ in range(self.iters):
            for ID, Xi in enumerate(X):
                cond = up_y[ID] * (np.dot(Xi, self.W) - self.B) >= 1
                if cond:
                    self.W -= self.lr * (2 * self.Lambda * self.W)
                else:
                    self.W -= self.lr * (2 * self.Lambda * self.W - np.multiply(Xi, up_y[ID]))
                    self.B -= self.lr * up_y[ID]


    def predict(self, X):
        val = np.dot(X, self.W) - self.B
        out = []
        for i in val:
            if i<=0:
                out.append(0)
            else:
                out.append(1)
        return out


#calculating accuracy
def accuracy(pred, data):
    mis_classified = 0
    for i in range(len(data)):
        if (data[i][0] != pred[i]):
            mis_classified +=1
    acc = 1 - mis_classified/len(data)
    return(acc)

#Performing Support Vector Machine our our dataset

svm = SVM()
svm.fit(data_trnsamp,data_trnlab)

y_pred = svm.predict(data_trnsamp)
out_trn = accuracy(y_pred,data_trnlab)
y_pred = svm.predict(data_testsamp)
out_test1 = accuracy(y_pred,data_testlab)
y_pred = svm.predict(data_testsamp2)
out_test2 = accuracy(y_pred,data_testlab2)
print("Support Vector Machine")
print("Training Accuracy : ",out_trn)
print("Testing Accuracy when door is closed: ",out_test1)
print("Testing accuracy when door is open: ",out_test2)
